#include <Arduino.h>

#include <uuid/log.h>

#include "driver/ledc.h"

#include "rf_ledc.h"

static uuid::log::Logger ledcLogger(F("ledc"));

RfLedc::RfLedc()
{
}

RfLedc::RfLedc(uint8_t pin, uint8_t channel, uint32_t freq, uint8_t res, uint16_t maxValue, float gamma)
{
    init(pin, channel, freq, res, maxValue, gamma);
}

void RfLedc::init(uint8_t pin, uint8_t channel, uint32_t freq, uint8_t res, uint16_t maxValue, float gamma)
{
    m_pin = pin;
    m_channel = channel;
    m_freq = freq;
    m_res = res;
    m_maxValue = maxValue;
    m_gamma = gamma;

    ledcSetup(m_channel, m_freq, m_res);
    ledcAttachPin(m_pin, m_channel);

    setValue(0);
}

uint16_t RfLedc::correctValue(uint16_t value)
{
    int resolution = pow(2, m_res) - 1;
	return(pow((float)value / (float)m_maxValue, m_gamma) * resolution + 0.5);
}

uint16_t RfLedc::setValue(uint16_t value)
{
    if(value>m_maxValue) {
        m_value = m_maxValue;
    } else {
        m_value = value;
    }

    uint16_t sv = correctValue(m_value);
    ledcLogger.trace("Value(%d): %d / %d\n", m_channel, m_value, sv);
    ledcWrite(m_channel, correctValue(m_value));

    return m_value;
}

void RfLedc::setGamma(float gamma)
{
    m_gamma = gamma;
}

float RfLedc::gamma() const
{
    return m_gamma;
}

uint16_t RfLedc::value() const
{
    return m_value;
}
