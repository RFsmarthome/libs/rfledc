#ifndef _RF_LEDC_H
#define _RF_LEDC_H

#include <Arduino.h>

class RfLedc
{
public:
    RfLedc();
    RfLedc(uint8_t pin, uint8_t channel, uint32_t freq, uint8_t res, uint16_t maxValue, float gamma);

    void init(uint8_t pin, uint8_t channel, uint32_t freq, uint8_t res, uint16_t maxValue, float gamma);

    uint16_t setValue(uint16_t value);
    void setGamma(float gamma);
    uint16_t value() const;
    float gamma() const;

protected:
    uint16_t correctValue(uint16_t value);

private:
    uint8_t m_pin;
    uint8_t m_channel;
    uint32_t m_freq;
    uint8_t m_res;
    uint16_t m_maxValue;
    float m_gamma;
    uint16_t m_value;
};

#endif
